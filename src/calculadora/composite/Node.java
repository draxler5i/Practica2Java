package calculadora.composite;

/**
 *
 * @author Daniel Merida
 */
public abstract class Node<T> {
    
    protected T data;
    
    public Node(T data) {
        this.data = data;
    }
    
    public String toString() {
        return "" + data;
    }
}
