package calculadora.visitor;

import calculadora.composite.Tree;
import calculadora.composite.Number;
import calculadora.composite.Node;
import calculadora.composite.Operator;
import java.util.ArrayList;

/**
 *
 * @author Daniel Merida
 */
public class BuildTree {

  private String[] ec;
  private ArrayList<Tree> arr;

  public BuildTree(String[] ec) {
    this.ec = ec;
    arr = new ArrayList<>();
  }

  public Tree builder() {
    for (int i = 0; i < ec.length; i++) {
      Node nodo;
      if (i % 2 == 1) {
        nodo = new Operator(ec[i]);
      } else {
        nodo = new Number(Double.parseDouble(ec[i]));
      }
      Tree tree = new Tree(nodo);
      arr.add(tree);
    }
    return builderOverload(0);
  }

  private Tree builderOverload(int i) {
    Tree res;
    if (!isOperation(arr.get(i).getRoot())) {
      if (i == arr.size() - 1) {
        res = arr.get(i);
        return res;
      }
      res = arr.get(i);
      Tree auxTree = builderOverload(i+1);
      auxTree.addChild(res);
      return auxTree;
    } else {
      res = arr.get(i);
      Tree auxTree = builderOverload(i+1);
      res.addChild(auxTree);
    }
    return res;
  }

  private boolean isOperation(Node nodo) {
    boolean res = false;
    if (nodo instanceof Operator) {
      res = true;
    }
    return res;
  }
}
