package calculadora.operations;

/**
 *
 * @author Daniel Merida
 */
public class LineEcu {

  private double x;
  private double y;
  private double var;

  public LineEcu(double x, double y, double var) {
    this.x = x;
    this.y = y;
    this.var = var;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public double getVar() {
    return var;
  }
}
