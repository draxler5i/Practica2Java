package calculadora.composite;

/**
 *
 * @author Daniel Merida
 */

public class Operator extends Node {
 
  private int weight;

  public Operator(String op) {
    super(op);
    giveWeight();
  }

  private void giveWeight() {
    if (data.equals("*") || data.equals("/")) {
      weight = 2;
    } else {
      weight = 1;
    }
  }

  public double getWeight() {
    return weight;
  }

  public void changeWeight(int newWeight) {
    weight = newWeight;
  }
}