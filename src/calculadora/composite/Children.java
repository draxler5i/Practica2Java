package calculadora.composite;

import java.util.ArrayList;

/**
 *
 * @author Daniel Merida
 */

public class Children {

    private ArrayList<Tree> children;

    public Children() {
        children = new ArrayList<>();
    }

    public void addChild(Tree child) {
        children.add(child);
    }

    public Integer getSize() {
        return children.size();
    }

    public Boolean isEmpty() {
        return children.isEmpty();
    }

    public Tree getChild(int i) {
        return children.get(i);
    }

    public String toString() {
        return ""+children.toString();
    }

    public void addChildLeft(Tree child) {
        children.add(0, child);
    }

    public void addChildRight(Tree child) {
        children.add(1, child);
    }
}
