package calculadora.operations;

import calculadora.composite.Tree;

/**
 *
 * @author Daniel Merida
 */
public class OperateTree {
    
    private Tree tree;
    
    public OperateTree(Tree tree) {
        this.tree = tree;
    }
    
    public double operateTree(){
        return operateTreeAux(tree);
    }
    
    private double operateTreeAux(Tree root) {
        double res;
        if (root.isLeaf()) {
            res = Double.parseDouble(root.getRoot().toString());
        } else {
            switch (root.getRoot().toString()) {
                case "*":
                    res = operateTreeAux(root.getChild(0)) * operateTreeAux(root.getChild(1));
                    break;
                case "/":
                    res = operateTreeAux(root.getChild(0)) / operateTreeAux(root.getChild(1));
                    break;
                case "+":
                    res = operateTreeAux(root.getChild(0)) + operateTreeAux(root.getChild(1));
                    break;
                default:
                    res = operateTreeAux(root.getChild(0)) - operateTreeAux(root.getChild(1));
                    break;
            }
        }
        return res;
    }
}
