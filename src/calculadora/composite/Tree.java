package calculadora.composite;

/**
 *
 * @author Daniel Merida
 * I want to use the composite pattern
 */

public class Tree {

  private Node root;
  private Children children;

  public Tree(Node root) {
    this.root = root;
    children = new Children();
  }

  public void addChild(Tree tree) {
    children.addChild(tree);
  }

  public void addChildLeft(Tree tree) {
    children.addChildLeft(tree);
  }

  public Children getChildren() {
    return children;
  }

  public Node getRoot() {
    return root;
  }

  public Boolean isLeaf() {
    return children.isEmpty();
  }

  public Tree getChild(int i) {
    return children.getChild(i);
  }

  public void addMeFather(Tree father) {
    father.addChild(this);
  }

  public String toString() {
    return ""+root;
  }
}
