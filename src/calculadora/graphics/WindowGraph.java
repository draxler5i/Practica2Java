package calculadora.graphics;

import calculadora.operations.LineEcu;
import calculadora.operations.MakePoints;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 *
 * @author Daniel Merida
 */
public class WindowGraph extends JFrame{

  private final int middlePointX = 350;
  private final int middlePointY = 300;
 
  private LineEcu line;
  
  public WindowGraph(LineEcu ecuation) {
    super();
    configure();
    line = ecuation;
    //initializeComponents();
  }

  private void configure() {
    this.setTitle("Calculadora");
    this.setSize(700, 600);
    this.setLocationRelativeTo(null);
    this.setVisible(true);
    this.setLayout(null);
    this.setResizable(true);
  }

  @Override
  public void paint(Graphics g) {
    g.translate(middlePointX, middlePointY);
    g.setColor(Color.RED);
    g.drawLine(0, -200, 0, 200); //Eje X
    g.setColor(Color.BLUE);
    g.drawLine(-250, 0, 250, 0); //Eje Y

    MakePoints points = new MakePoints(line);
    int p1X, p1Y, p2X, p2Y;
    int [] p1 = points.getPoint1();
    int [] p2 = points.getPoint2();
    p1X = p1[0];
    p1Y = p1[1];
    p2X = p2[0];
    p2Y = p2[1];
    g.setColor(Color.green);
    g.drawLine(p1X, p1Y, p2X, p2Y);
  }
}
