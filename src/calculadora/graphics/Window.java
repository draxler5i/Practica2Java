package calculadora.graphics;

import calculadora.composite.Tree;
import calculadora.operations.LineEcu;
import calculadora.operations.OperateTree;
import calculadora.visitor.BuildTree;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Daniel Merida
 */
public class Window extends JFrame implements ActionListener{

  private JLabel text;
  private JTextField box;
  private JButton btnGraph;
  private JButton btnCalc;

  public Window() {
    super();
    configure();
    initializeComponents();
  }

  private void configure() {
    this.setTitle("Calculadora");
    this.setSize(310, 210);
    this.setLocationRelativeTo(null);
    this.setVisible(true);
    this.setLayout(null);
    this.setResizable(false);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void initializeComponents() {
    text = new JLabel();
    box = new JTextField();
    btnCalc = new JButton();
    btnGraph = new JButton();

    text.setText("Inserte ecuacion");
    text.setBounds(100, 25, 100, 25);
    box.setBounds(100, 50, 100, 25);
    btnCalc.setText("Calculate");
    btnCalc.setBounds(50, 100, 200, 30);
    btnCalc.addActionListener(this);
    btnGraph.setText("Graph");
    btnGraph.setBounds(50, 140, 200, 30);
    btnGraph.addActionListener(this);

    this.add(text);
    this.add(box);
    this.add(btnCalc);
    this.add(btnGraph);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == btnGraph) {
      String[] input = box.getText().split(" ");
      double x = Double.parseDouble(input[0]);
      double y = Double.parseDouble(input[1]);
      double var = Double.parseDouble(input[2]);
      LineEcu ecu = new LineEcu(x, y, var);
      
      WindowGraph graph = new WindowGraph(ecu);
      
    } else {
      String[] input = box.getText().split(" ");
      BuildTree tree = new BuildTree(input);
      Tree myTree = tree.builder();
      OperateTree op = new OperateTree(myTree);
      JOptionPane.showMessageDialog(this, "Respuesta: "+op.operateTree());
    }
  }
}
