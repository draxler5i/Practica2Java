package calculadora.operations;

/**
 *
 * @author Daniel Merida
 */
public class MakePoints {

  private LineEcu line;

  public MakePoints(LineEcu line){
    this.line = line;
  }

  public int[] getPoint1() {
    int[] point1;
    int x = (int) line.getX();
    int y = (int) line.getY() * (-1);
    int var = (int) line.getVar() * (-1);
    y = y * 1; //para y = 1;
    x = (y + var)/x;
    y = 1;
    point1 = translatePoints(x, y); 
    return point1;
  }

  public int[] getPoint2() {
    int[] point2;
    int x = (int) line.getX();
    int y = (int) line.getY() * (-1);
    int var = (int) line.getVar() * (-1);
    y = y * 20; //para y = 20;
    x = (y + var)/x;
    y = (int) (y * line.getY() * (-1));
    point2 = translatePoints(x, y);
    return point2;
  }

  private int[] translatePoints(int x, int y) {
    int[] res = new int[2];
    x = x * 10;
    y = y * (-10);
    res[0] = x;
    res [1] = y;
    return res;
  }
}
